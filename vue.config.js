module.exports = {
  publicPath: '/',
  // indexPath:'index.html',
  devServer: {
    overlay: {
      warnings: false,
      errors: false
    },
    proxy: {
      '/': { //
        target: 'http://192.168.1.24:80',
        changeOrigin: true
      }
    }
  },
  lintOnSave: false,
  configureWebpack: {
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      }
    }
  }
}
